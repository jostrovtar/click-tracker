package si.rovtar.clicktracker.endpoints;

import com.google.api.server.spi.response.UnauthorizedException;
import org.apache.commons.codec.binary.Base64;
import si.rovtar.clicktracker.core.Constants;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

/**
 * Created by Jost on 4.7.2016.
 */
public class Authenticator {

    private static final Logger log = Logger.getLogger(Authenticator.class.getName());

    public static void authenticateUser(HttpServletRequest request) throws UnauthorizedException {

        if(request == null){
            throw new UnauthorizedException("Missing request data");
        }

        String authHeaderValue = request.getHeader("authorization");

        if(authHeaderValue == null){
            throw new UnauthorizedException("Missing authorization data");
        }

        String[] list = authHeaderValue.split(" ");
        // Value should be something like: Basic am9zdDpnZXNsbw==

        if(list.length != 2){
            throw new UnauthorizedException("Authorization header value is not valid");
        }

        String authCredentials = new String(Base64.decodeBase64(list[1]));
        // Value should be username:password

        String[] credentials = authCredentials.split(":");

        if(credentials.length != 2){
            throw new UnauthorizedException("User credentials could not be parsed correctly");
        }

        if(!credentials[0].equals(Constants.USERNAME) || !credentials[1].equals(Constants.PASSWORD)){
            throw new UnauthorizedException("Invalid username or password");
        }

        log.info("User " + credentials[0] + " successfully authenticated");

    }

}
