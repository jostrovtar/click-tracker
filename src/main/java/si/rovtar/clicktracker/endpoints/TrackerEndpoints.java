package si.rovtar.clicktracker.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import si.rovtar.clicktracker.core.ClickValidation;
import si.rovtar.clicktracker.exceptions.EntityNotFoundException;
import si.rovtar.clicktracker.exceptions.MissingDataException;
import si.rovtar.clicktracker.model.RedirectUrl;
import si.rovtar.clicktracker.model.TrackingLink;
import si.rovtar.clicktracker.model.UserPlatform;


@Api(name = "trackerendpoints", version = "v1")
public class TrackerEndpoints {

    @ApiMethod(name = "trackClick", path = "trackclick", httpMethod = ApiMethod.HttpMethod.GET)
    public RedirectUrl trackClick(@Named("campaignId") long campaignId, @Named("userPlatform") UserPlatform userPlatform) throws MissingDataException, EntityNotFoundException {

        TrackingLink tl = new TrackingLink();
        tl.setCampaignId(campaignId);
        tl.setPlatform(userPlatform);

        return ClickValidation.validateClick(tl);
    }

}

