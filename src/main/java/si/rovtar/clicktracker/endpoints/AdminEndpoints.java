package si.rovtar.clicktracker.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.UnauthorizedException;
import si.rovtar.clicktracker.dao.CampaignDAO;
import si.rovtar.clicktracker.dao.TrackingLinkDAO;
import si.rovtar.clicktracker.exceptions.EntityNotFoundException;
import si.rovtar.clicktracker.exceptions.MissingDataException;
import si.rovtar.clicktracker.model.Campaign;
import si.rovtar.clicktracker.model.NumberOfClicks;
import si.rovtar.clicktracker.model.UserPlatform;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.logging.Logger;


@Api(name = "adminendpoints", version = "v1")
public class AdminEndpoints {

    private static final Logger log = Logger.getLogger(AdminEndpoints.class.getName());

    @ApiMethod(name = "createcampaign", path = "createcampaign", httpMethod = ApiMethod.HttpMethod.POST)
    public Campaign createCampaign(HttpServletRequest request, Campaign c) throws MissingDataException, UnauthorizedException {

        Authenticator.authenticateUser(request);

        if (c == null) {
            String message = "Missing parameter campaign";
            log.info(message);
            throw new MissingDataException(message);
        } else if (c.getPlatforms() == null || c.getPlatforms().isEmpty()) {
            String message = "Missing campaign parameter platforms";
            log.info(message);
            throw new MissingDataException(message);
        } else if (c.getRedirectUrl() == null || c.getRedirectUrl().length() == 0) {
            String message = "Missing campaign parameter redirectUrl";
            log.info(message);
            throw new MissingDataException(message);
        }

        return CampaignDAO.saveCampaign(c);
    }

    @ApiMethod(name = "getcampaign", path = "campaign/{id}", httpMethod = ApiMethod.HttpMethod.GET)
    public Campaign getCampaign(HttpServletRequest request, @Named("id") long campaignId)
            throws EntityNotFoundException, UnauthorizedException {

        Authenticator.authenticateUser(request);

        return CampaignDAO.getCampaign(campaignId);
    }

    @ApiMethod(name = "updatecampaign", path = "updatecampaign", httpMethod = ApiMethod.HttpMethod.PUT)
    public Campaign updateCampaign(HttpServletRequest request, Campaign c)
            throws MissingDataException, UnauthorizedException {

        Authenticator.authenticateUser(request);

        if (c == null) {
            String message = "Missing parameter campaign";
            log.info(message);
            throw new MissingDataException(message);
        } else if (c.getId() <= 0) {
            String message = "Missing campaign parameter id";
            log.info(message);
            throw new MissingDataException(message);
        } else if (c.getPlatforms() == null || c.getPlatforms().isEmpty()) {
            String message = "Missing campaign parameter platforms";
            log.info(message);
            throw new MissingDataException(message);
        } else if (c.getRedirectUrl() == null || c.getRedirectUrl().length() == 0) {
            String message = "Missing campaign parameter redirectUrl";
            log.info(message);
            throw new MissingDataException(message);
        }

        return CampaignDAO.updateCampaign(c);
    }


    @ApiMethod(name = "deletecampaign", path = "campaign/{id}", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void deleteCampaign(HttpServletRequest request, @Named("id") long campaignId)
            throws EntityNotFoundException, UnauthorizedException {

        Authenticator.authenticateUser(request);

        CampaignDAO.deleteCampaign(campaignId);
    }

    @ApiMethod(name = "getCampaignsOnPlatform", path = "campaignsonplatform", httpMethod = ApiMethod.HttpMethod.GET)
    public List<Campaign> getCampaignsOnPlatform(HttpServletRequest request, @Named("platform") UserPlatform platform)
            throws UnauthorizedException {

        Authenticator.authenticateUser(request);

        return CampaignDAO.getCampaignsOnPlatform(platform);
    }


    @ApiMethod(name = "numClicksForCampaignOnPlatform", path = "numclicksforcampaignonplatform", httpMethod = ApiMethod.HttpMethod.GET)
    public NumberOfClicks numClicksForCampaignOnPlatform(HttpServletRequest request, @Named("campaignId") long campaignId, @Named("platform") UserPlatform platform)
            throws UnauthorizedException {

        Authenticator.authenticateUser(request);

        return TrackingLinkDAO.numClicksForCampaignOnPlatform(campaignId, platform);
    }

    @ApiMethod(name = "numClicksOnPlatform", path = "numclicksonplatform", httpMethod = ApiMethod.HttpMethod.GET)
    public NumberOfClicks numClicksOnPlatform(HttpServletRequest request, @Named("platform") UserPlatform platform)
            throws UnauthorizedException {

        Authenticator.authenticateUser(request);

        return TrackingLinkDAO.numClicksOnPlatform(platform);
    }


}

