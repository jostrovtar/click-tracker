package si.rovtar.clicktracker.exceptions;

import com.google.api.server.spi.ServiceException;

public class MissingDataException extends ServiceException {

    public MissingDataException(String message) {
        super(400, message);
    }

}
