package si.rovtar.clicktracker.exceptions;

import com.google.api.server.spi.ServiceException;

public class EntityNotFoundException extends ServiceException {

    public EntityNotFoundException(String message) {
        super(400, message);
    }

}
