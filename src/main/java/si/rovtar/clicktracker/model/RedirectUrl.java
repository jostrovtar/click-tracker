package si.rovtar.clicktracker.model;

public class RedirectUrl {

    private String url;

    public RedirectUrl() {
    }

    public RedirectUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
