package si.rovtar.clicktracker.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;

import java.util.Date;
import java.util.List;

@Entity
public class Campaign {

    @Id
    private long id;
    private String redirectUrl;
    @Ignore
    private List<CampaignOnPlatform> platforms;
    private Date creationDate;
    private Date updateDate;

    public Campaign() {
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getRedirectUrl() {

        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {

        this.redirectUrl = redirectUrl;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }


    public List<CampaignOnPlatform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<CampaignOnPlatform> platforms) {
        this.platforms = platforms;
    }


}
