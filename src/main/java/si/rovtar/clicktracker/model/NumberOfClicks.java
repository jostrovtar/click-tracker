package si.rovtar.clicktracker.model;

public class NumberOfClicks {

    private long num;

    public NumberOfClicks(){}

    public NumberOfClicks(long num){
        this.num = num;
    }

    public long getNum() {
        return num;
    }

    public void setNum(long num) {
        this.num = num;
    }
}
