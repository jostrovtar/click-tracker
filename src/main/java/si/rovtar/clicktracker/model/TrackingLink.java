package si.rovtar.clicktracker.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Date;

@Entity
public class TrackingLink {

    @Id
    private long id;
    @Index
    private long campaignId;
    @Index
    private UserPlatform platform;


    private Date clickDate;

    public TrackingLink() {
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public long getCampaignId() {

        return campaignId;
    }

    public void setCampaignId(long campaignId) {

        this.campaignId = campaignId;
    }

    public UserPlatform getPlatform() {

        return platform;
    }

    public void setPlatform(UserPlatform platform) {

        this.platform = platform;
    }

    public Date getClickDate() {
        return clickDate;
    }

    public void setClickDate(Date clickDate) {
        this.clickDate = clickDate;
    }

}
