package si.rovtar.clicktracker.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class CampaignOnPlatform {

    @Id
    private long id;
    @Index
    private long campaignId;
    @Index
    private UserPlatform platform;

    public CampaignOnPlatform(){}

    public CampaignOnPlatform(long campaignId, UserPlatform platform){
        this.campaignId = campaignId;
        this.platform = platform;
    }

    public UserPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(UserPlatform platform) {
        this.platform = platform;
    }

    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


}
