package si.rovtar.clicktracker.dao;

import si.rovtar.clicktracker.model.Campaign;
import si.rovtar.clicktracker.model.CampaignOnPlatform;
import si.rovtar.clicktracker.model.UserPlatform;

import java.util.List;
import java.util.logging.Logger;

import static si.rovtar.clicktracker.dao.OfyService.factory;
import static si.rovtar.clicktracker.dao.OfyService.ofy;

public class CampaignOnPlatformDAO {

    private static final Logger log = Logger.getLogger(CampaignOnPlatformDAO.class.getName());


    public static void saveList(List<CampaignOnPlatform> list, long campaignId) {

        log.info("Saving CampaignOnPlatform for campaign id: " + campaignId);

        for (CampaignOnPlatform cop : list) {
            cop.setId(factory().allocateId(CampaignOnPlatform.class).getId());
            cop.setCampaignId(campaignId);
            ofy().save().entity(cop).now();
        }

    }

    public static List<CampaignOnPlatform> getForCampaign(long campaignId) {
        log.info("Getting CampaignOnPlatform for campaign id: " + campaignId);
        return ofy().load().type(CampaignOnPlatform.class).filter("campaignId", campaignId).list();
    }

    public static List<CampaignOnPlatform> getForPlatform(UserPlatform userPlatform) {
        log.info("Getting CampaignOnPlatform for platform id: " + userPlatform);
        return ofy().load().type(CampaignOnPlatform.class).filter("platform", userPlatform).list();
    }

    public static void updateForCampaign(Campaign c) {
        log.info("Updating CampaignOnPlatform for campaign id: " + c.getId());
        deleteForCampaign(c.getId());
        saveList(c.getPlatforms(), c.getId());
    }

    public static void deleteForCampaign(long campaignId) {

        log.info("Deleting CampaignOnPlatform for campaign id: " + campaignId);
        for (CampaignOnPlatform cop : getForCampaign(campaignId)) {
            ofy().delete().entity(cop).now();
        }

    }


}
