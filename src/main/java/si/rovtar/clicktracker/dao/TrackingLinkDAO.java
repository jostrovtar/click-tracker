package si.rovtar.clicktracker.dao;


import si.rovtar.clicktracker.model.NumberOfClicks;
import si.rovtar.clicktracker.model.TrackingLink;
import si.rovtar.clicktracker.model.UserPlatform;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import static si.rovtar.clicktracker.dao.OfyService.factory;
import static si.rovtar.clicktracker.dao.OfyService.ofy;

public class TrackingLinkDAO {

    private static final Logger log = Logger.getLogger(TrackingLinkDAO.class.getName());

    public static void saveTrackingLink(TrackingLink link) {

        link.setId(factory().allocateId(TrackingLink.class).getId());
        link.setClickDate(new Date());

        ofy().save().entity(link).now();

        log.info("Created new TrackingLink with id " + link.getId());

    }

    public static NumberOfClicks numClicksForCampaignOnPlatform(long campaignId, UserPlatform platform) {

        return new NumberOfClicks(ofy().load().type(TrackingLink.class).filter("campaignId", campaignId).filter("platform", platform).count());
    }

    public static NumberOfClicks numClicksOnPlatform( UserPlatform platform) {

        return new NumberOfClicks(ofy().load().type(TrackingLink.class).filter("platform", platform).count());
    }


}
