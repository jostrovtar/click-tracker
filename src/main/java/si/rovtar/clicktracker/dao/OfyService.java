package si.rovtar.clicktracker.dao;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import si.rovtar.clicktracker.model.Campaign;
import si.rovtar.clicktracker.model.CampaignOnPlatform;
import si.rovtar.clicktracker.model.TrackingLink;

import java.util.logging.Logger;

public class OfyService {

    private static final Logger log = Logger.getLogger(OfyService.class.getName());

    static {

        factory().register(Campaign.class);
        factory().register(TrackingLink.class);
        factory().register(CampaignOnPlatform.class);

        log.info("Loaded Services objects for app OfyService call");

    }


    public static Objectify ofy() {

        ObjectifyService.begin();
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

}
