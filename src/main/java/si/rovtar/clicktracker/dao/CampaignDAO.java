package si.rovtar.clicktracker.dao;

import si.rovtar.clicktracker.exceptions.EntityNotFoundException;
import si.rovtar.clicktracker.model.Campaign;
import si.rovtar.clicktracker.model.CampaignOnPlatform;
import si.rovtar.clicktracker.model.UserPlatform;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import static si.rovtar.clicktracker.dao.OfyService.factory;
import static si.rovtar.clicktracker.dao.OfyService.ofy;

public class CampaignDAO {

    private static final Logger log = Logger.getLogger(CampaignDAO.class.getName());

    public static Campaign getCampaign(long id) throws EntityNotFoundException {

        Campaign c = ofy().load().type(Campaign.class).id(id).now();

        if (c != null) {
            c.setPlatforms(CampaignOnPlatformDAO.getForCampaign(c.getId()));
        } else {
            String message = "Campaign with id " + id + " not found";
            log.info(message);
            throw new EntityNotFoundException(message);
        }

        log.info("Returning Campaign with id " + id);

        return c;
    }

    public static Campaign saveCampaign(Campaign c) {

        c.setId(factory().allocateId(Campaign.class).getId());
        c.setCreationDate(new Date());
        c.setUpdateDate(new Date());

        CampaignOnPlatformDAO.saveList(c.getPlatforms(), c.getId());

        ofy().save().entity(c).now();

        log.info("Created new campaign with id " + c.getId());

        return c;
    }

    public static Campaign updateCampaign(Campaign c) {

        c.setUpdateDate(new Date());
        ofy().save().entity(c).now();

        CampaignOnPlatformDAO.updateForCampaign(c);

        log.info("Updated campaign with id " + c.getId());

        return c;
    }

    public static void deleteCampaign(long campaignId) throws EntityNotFoundException {

        Campaign c = getCampaign(campaignId);

        ofy().delete().entity(c).now();
        CampaignOnPlatformDAO.deleteForCampaign(campaignId);

        log.info("Deleted campaign with id " + campaignId);
    }

    public static List<Campaign> getCampaignsOnPlatform(UserPlatform platform){

        List<CampaignOnPlatform> listCop = CampaignOnPlatformDAO.getForPlatform(platform);
        List<Campaign> listc = new ArrayList<Campaign>();

        for(CampaignOnPlatform cop : listCop){
            try {
                listc.add(getCampaign(cop.getCampaignId()));
            } catch(EntityNotFoundException e){
                log.info("Error while retrieving Campaign with id " + cop.getCampaignId());
            }
        }

        return listc;
    }




}
