package si.rovtar.clicktracker.core;

import si.rovtar.clicktracker.dao.CampaignDAO;
import si.rovtar.clicktracker.dao.TrackingLinkDAO;
import si.rovtar.clicktracker.exceptions.EntityNotFoundException;
import si.rovtar.clicktracker.model.*;

import java.util.List;
import java.util.logging.Logger;

public class ClickValidation {

    private static final Logger log = Logger.getLogger(ClickValidation.class.getName());

    public static RedirectUrl validateClick(TrackingLink trackingLink) {

        RedirectUrl redirect = new RedirectUrl();

        try {

            Campaign c = CampaignDAO.getCampaign(trackingLink.getCampaignId());

            if (c != null && c.getPlatforms() != null && campaignAvelableOnPlatform(c.getPlatforms(), trackingLink.getPlatform())) {
                redirect.setUrl(c.getRedirectUrl());
                TrackingLinkDAO.saveTrackingLink(trackingLink);
                log.info("Click campaign id " + trackingLink.getCampaignId() + " and platform "
                        + trackingLink.getPlatform() + " is successfully validated");
            } else {
                redirect.setUrl(Constants.INVALID_CLICK_REDIRECT_URL);
                log.info("Click campaign id " + trackingLink.getCampaignId() + " and platform "
                        + trackingLink.getPlatform() + " is NOT VALID");
            }

        } catch (EntityNotFoundException e) {
            redirect.setUrl(Constants.INVALID_CLICK_REDIRECT_URL);
            log.info("Click campaign id " + trackingLink.getCampaignId() + " and platform "
                    + trackingLink.getPlatform() + " is NOT VALID");
        }

        return redirect;
    }

    private static boolean campaignAvelableOnPlatform(List<CampaignOnPlatform> campaignOnPlatforms, UserPlatform platform) {

        for (CampaignOnPlatform cop : campaignOnPlatforms) {
            if (cop.getPlatform().equals(platform)) {
                return true;
            }
        }

        return false;
    }

}
