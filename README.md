# Click tracker#

Click tracker is backend application responsible for tracking all clicks that were made on mobile clients. 
It provides tracking links instead of direct urls. Tracking links are urls that point to our server and contain information about the click. When user clicks the link, he is provided with the intended web address and information about that link is stored for analytical purposes.

Every click is always in the context of a given campaign. Campaign is the main entity of the system and it must have defined redirect url and platform it is enabled on. Currently avaliable platforms are Android (ANDROID), IPhone (IPHONE) and Windows phone (WP). 

### Environment setup ###
Click tracker is Java web application and is designed to run on Google Cloud Appengine. It has REST API which is implemented with google Cloud Endpoints and uses Google Cloud Datastore database for saving data.  It is built with Maven project management tool. In order to run application locally or deploy it to google cloud, environemtn has to be properly set up:

* Install [Java 7](http://www.java.com/en/download/manual.jsp)
* [Install](https://maven.apache.org/download.cgi) and [configure](https://maven.apache.org/install.html) Apache Maven


### Running application on development server ###

Move to the home directory of the project (where the pom.xml file is).

* type: **mvn clean appengine:devserver**


### Deploying application to Google cloud ###

* Create a project in [Google Cloud Console](https://cloud.google.com/console)
* edit file **path_to_project_home/src/main/webepp/WEB-INF/appengine-web.xml** and change value **application-id** with id that was generated when you created project on Google Cloud Console
* go to home directory of the project and type **mvn clean appengine:update**


## Exposed APIs ##
Application expose two apies.


### Tracker API ###
Main api through which mobile clients interact with the backend application.

Exposed Method:

* url: _ah/api/trackerendpoints/v1/trackclick
* http method: GET
* required named parameter: campaignId
* required named parameter: userPlatform
* example: _ah/api/trackerendpoints/v1/trackclick?campaignId=2010001&userPlatform=IPHONE



### Admin API ###
Created for administrators to manage campaigns and check current statistic. All methods are secured with **basic authentication**. Default username and password are **admin** and **admin1!**. To change them open file path_to_project/src/main/java/si/rovtar/clicktracker/core/Constants.java and change variables username and password. After change is made, project must be recompiled.

Exposed Methods:

**Creation of new campaign**

* url: /_ah/api/adminendpoints/v1/createcampaign
* http method: POST
* required data param
* data param example (type application/json):
```
       {
         "redirectUrl" : "http://redirect.url22.com",
         "platforms" : [ {
              "platform" : "ANDROID"
              }
          ]
       }
```
* response example:
```
{
  "id" : "1",
  "redirectUrl" : "http://redirect.url2.com",
  "platforms" : [ {
    "id" : "2",
    "campaignId" : "1",
    "platform" : "ANDROID"
     }
   ],
  "creationDate" : "2016-07-05T13:38:47.927+02:00",
  "updateDate" : "2016-07-05T13:38:47.927+02:00"
}
```

**Update of the existing campaign**

* url: /_ah/api/adminendpoints/v1/updatecampaign
* http method: PUT
* required data param
* data param example (type application/json):
```
{
  "id" : "1",
  "redirectUrl" : "http://redirect.url2.com",
  "platforms": [{
   "platform": "ANDROID"
  },{
   "platform": "IPHONE"
  },{
   "platform": "WP"
  }
 ]
}
```
* response example:
```
{
  "id" : "1",
  "redirectUrl" : "http://redirect.url2.com",
  "platforms": [
  {
   "id": "4",
   "campaignId": "1",
   "platform": "ANDROID"
  },
  {
   "id": "5",
   "campaignId": "1",
   "platform": "IPHONE"
  },
  {
   "id": "6",
   "campaignId": "1",
   "platform": "WP"
  }
 ],
  "creationDate" : "2016-07-05T13:38:47.927+02:00",
  "updateDate" : "2016-07-05T13:38:47.927+02:00"
}
```

**Deletion of the existing campaign**

* url: /_ah/api/adminendpoints/v1/campaign/{id}
* http method: DELETE
* required path param: id
* example: /_ah/api/adminendpoints/v1/campaign/1
* in case of successfull deletion no dta is returned


**Displaying information about the existing campaign**

* url: /_ah/api/adminendpoints/v1/campaign/{id}
* http method: GET
* required path param: id
* example: /_ah/api/adminendpoints/v1/campaign/1
* response example:
```
{
  "id" : "1",
  "redirectUrl" : "http://redirect.url2.com",
  "platforms": [
  {
   "id": "4",
   "campaignId": "1",
   "platform": "ANDROID"
  },
  {
   "id": "5",
   "campaignId": "1",
   "platform": "IPHONE"
  },
  {
   "id": "6",
   "campaignId": "1",
   "platform": "WP"
  }
 ],
  "creationDate" : "2016-07-05T13:38:47.927+02:00",
  "updateDate" : "2016-07-05T13:38:47.927+02:00"
}
```

**Listing all existing campaigns available on given platform**

* url: /_ah/api/adminendpoints/v1/campaignsonplatform
* http method: GET
* required named param: platform
* example: /_ah/api/adminendpoints/v1/campaignsonplatform?platform=ANDROID
* response example:
```
{
  "items" : [ {
    "id" : "1",
    "redirectUrl" : "http://redirect.url3.com",
    "platforms" : [ {
      "id" : "2",
      "campaignId" : "1",
      "platform" : "ANDROID"
    } ],
    "creationDate" : "2016-07-05T10:34:37.379+02:00",
    "updateDate" : "2016-07-05T10:34:37.379+02:00"
  }, {
    "id" : "3",
    "redirectUrl" : "http://redirect.url2.com",
    "platforms" : [ {
      "id" : "4",
      "campaignId" : "3",
      "platform" : "ANDROID"
    }, {
      "id" : "5",
      "campaignId" : "3",
      "platform" : "IPHONE"
    } ],
    "creationDate" : "2016-07-05T10:34:44.261+02:00",
    "updateDate" : "2016-07-05T10:34:44.261+02:00"
  }
}
```

**Retrieving number of clicks for given campaign on the given platform**

* url: /_ah/api/adminendpoints/v1/numclicksforcampaignonplatform
* http method: GET
* required named param: campaignId
* required named param: platform
* example: /_ah/api/adminendpoints/v1/numclicksforcampaignonplatform?campaignId=2&platform=ANDROID
* response example:
```
{
  "num" : "5"
}
```



**Retrieving number of clicks on the given platform**

* url: /_ah/api/adminendpoints/v1/numclicksonplatform
* http method: GET
* required named param: platform
* example: /_ah/api/adminendpoints/v1/numclicksonplatform?platform=ANDROID
* response example:
```
{
  "num" : "10"
}
```